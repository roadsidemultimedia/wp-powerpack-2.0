# WP Power Pack 2.0

## Additional Functionality & Modifications

- Adds a filter that prevents WordPress update emails from being sent
- Adds classes to the body of each page with the parent page ID and ancestor page ID in the following format: `parent-page-{$ancestor_id}` and `ancestor-page-{$ancestor_id}`
- Adds support for uploading `.ico` and `.ogv` files with the Media Uploader.
- Allows users with Author role to view, edit and comment on Gravity Form entries. This is normally only accessible to admin users.
- Adds a filter that prevents Google Apps Login plugin from setting cookies on every single page request, which could potentially cause caching issues, and restricts it to the login screen.
- Converts Gravity Forms submit input into an actual button element for easier styling
- Functionality that converts Gravity Forms headings (H2 and H3) to divs for better SEO and styling

## Shortcodes

- `[custom_menu]` shortcode allows you to display custom menus anywhere that shortcodes work. Accepts all the attributes of [wp_nav_menu](https://developer.wordpress.org/reference/functions/wp_nav_menu/).
- Adds a shortcode for dynamic copywrite date [copyright_date] which outputs the current year.


## Functions

### is_tree( $id )
`is_tree( $id )` allows you to use conditional logic to test if the current page is a descendant of another page, or a set of pages, no matter how many levels up. This function accepts either a single ID or an array of IDs.
This function can be used in Widget Logic.


- **Example #1**  
Check if the current page is a descendant of a page with the ID of 64.

    ```
    is_tree( 64 )
    ```

- **Example #2**  
Check if the current page is a descendant of several pages (123, 124, or 225).

    ```
    is_tree( array(123, 124, 225) )
    ```


### is_child( $id )
`is_child( $id )` checks if current page is a direct child of specified ID. Can be used in Widget Logic.


### has_siblings( $id )
- Adds function `has_siblings( $id )` to check if page has siblings. Can be used in Widget Logic.


## Todo

* Add caching to the `[copyright_date]` code that checks calculates the year of the oldest post on the site to the present date, and produces a result such as: 2010-2012. For now, it only displays the current year.