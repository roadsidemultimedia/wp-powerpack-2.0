<?php
/*
Plugin Name: WP Power Pack 2.0
Description: Wordpress power ups for your web development!
Version: 2.0.7
Author: Roadside Multimedia
Contributors: Milo Jennings, Curtis Grant, Barak Llewellyn
Plugin URI: https://bitbucket.org/roadsidemultimedia/wp-powerpack-2.0
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/wp-powerpack-2.0
Bitbucket Branch: master
License: GPL2
Text Domain: wp-pp2
*/

/* 
 * NOTE: Admin page for future use 
 */
// include_once ( dirname( __FILE__ ) . "/admin/admin-functions.php" );
// include_once ( dirname( __FILE__ ) . "/admin/admin-page.php" );

define('WP_POWERPACK_PATH', plugin_dir_path(__FILE__) );
define('WP_POWERPACK_INDEX_PATH', __FILE__);

include_once ( dirname( __FILE__ ) . "/lib/shortcodes.php" );
include_once ( dirname( __FILE__ ) . "/lib/utilities.php" );
include_once ( dirname( __FILE__ ) . "/lib/functionality.php" );
include_once ( dirname( __FILE__ ) . "/lib/gravity-forms-permissions.php" );
include_once ( dirname( __FILE__ ) . "/lib/gravity-forms-button-change.php" );
include_once ( dirname( __FILE__ ) . "/lib/gravity-forms-headings-to-divs.php" );
include_once ( dirname( __FILE__ ) . "/lib/cleanup-wp-dashboard.php" );
include_once ( dirname( __FILE__ ) . "/lib/wp-rocket-fix.php" );
include_once ( dirname( __FILE__ ) . "/lib/gravitar-alt-tag.php" );
include_once ( dirname( __FILE__ ) . "/lib/simple-history.php" );
include_once ( dirname( __FILE__ ) . "/lib/google-apps-login-cookie-fix.php" );
