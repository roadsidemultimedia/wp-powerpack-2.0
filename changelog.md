## Changelog

### Version 2.0.7 [04/13/2020]

- Added functionality that converts Gravity Forms submit input into an actual button element for easier styling. Harvested from "Gform Button Change" plugin created by Curtis.
- Added functionality that converts Gravity Forms headings to divs for better SEO and styling

### Version 2.0.6 [05/29/2018]

- Added a filter that prevents Google Apps Login plugin from setting cookies on every single page request, which could potentially cause caching issues, and restricts it to the login screen.

### Version 2.0.5 [02/01/2018]

- Added a filer that lengthens the Simple History plugin's log from 60 days to 180 days

### Version 2.0.4 [06/21/2017]

- Alt tags are added to Gravitars now.

### Version 2.0.3 [06/14/2017]

- Added fix for WP Rocket caching issue.

### Version 2.0.2 [06/07/2017]

- Removed `firstChild_redirect_filter` since it causes conflicts with MyDivi theme, and is already included in both DMS Skeleton and MyDivi themes. Most of our sites should already have it.

### Version 2.0.1 [03/08/2017]

- Added code that allows users with Author role to view, edit and comment on Gravity Form entries.
- Added code that removes excess widgets from the WordPress dashboard (Yoast SEO (only hidden for non-admins), CSS & Javascript overview widget, quickdraft, wordpress news, at a glance, recent drafts, etc...).

### Version 2.0.0 [01/24/2017]

 - Extracted useful functionality from old [WP Power Pack](https://bitbucket.org/roadsidemultimedia/wp-power-pack-plugin), and created this new plugin
 - Added filter to prevent WordPress update emails from being sent