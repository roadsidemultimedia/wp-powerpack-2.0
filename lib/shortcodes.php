<?php

// Function that will return our Wordpress menu
if(!function_exists("custom_menu_shortcode")){
	function custom_menu_shortcode($atts, $content = null) {
		extract(shortcode_atts(array(  
			'menu'            => '',
			'container'       => 'false',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => 'custom_menu',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'depth'           => 0,
			'walker'          => '',
			'theme_location'  => ''), 
			$atts));
 
 
		return wp_nav_menu( array( 
			'menu'            => $menu, 
			'container'       => $container, 
			'container_class' => $container_class, 
			'container_id'    => $container_id, 
			'menu_class'      => $menu_class, 
			'menu_id'         => $menu_id,
			'echo'            => false,
			'fallback_cb'     => $fallback_cb,
			'before'          => $before,
			'after'           => $after,
			'link_before'     => $link_before,
			'link_after'      => $link_after,
			'depth'           => $depth,
			'walker'          => $walker,
			'theme_location'  => $theme_location));
	}
	//Create the shortcode
	add_shortcode("custom_menu", "custom_menu_shortcode");
}

// Dynamic copyright date
if( !function_exists("dynamic_copyright_date_shortcode") && !shortcode_exists( 'copyright_date' ) ){
	function dynamic_copyright_date_shortcode($atts, $content = null) {
		// global $wpdb;
		// $copyright_dates = $wpdb->get_results("
		// 	SELECT
		// 	YEAR(min(post_date_gmt)) AS firstdate,
		// 	YEAR(max(post_date_gmt)) AS lastdate
		// 	FROM
		// 	$wpdb->posts
		// 	WHERE
		// 	post_status = 'publish' AND post_type = 'post'
		// ");
		// $output = '';
		// if($copyright_dates) {
		// 	$copyright = $copyright_dates[0]->firstdate;
		// 	$copyright_dates[0]->lastdate = date('Y');
		// 	if($copyright_dates[0]->firstdate != $copyright_dates[0]->lastdate) {
		// 		$copyright .= '-' . $copyright_dates[0]->lastdate;
		// 	}
		// 	$output = $copyright;
		// }
		// return $output;
		return date('Y');
	}
	add_shortcode("copyright_date", "dynamic_copyright_date_shortcode");
}