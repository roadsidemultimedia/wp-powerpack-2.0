<?php
function pp2_add_gravatar_alt($text) {
  $alt = get_the_author_meta( 'display_name' );
  $text = str_replace('alt=\'\'', 'alt=\'Avatar for '.$alt.'\' title=\'Gravatar for '.$alt.'\'',$text);
  return $text;
}
add_filter('get_avatar','pp2_add_gravatar_alt');