<?php
function powerpack__remove_dashboard_widgets() {
  global $wp_meta_boxes;

  //Remove WordPress default dashboard widgets
  remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
  remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
  remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
  remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
  remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
  remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
  remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
  remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
  
  // Only hide if not an admin
  if ( ! current_user_can('administrator') ) { 
    remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
    remove_meta_box( 'cjt-statistics', 'dashboard', 'core');
    remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'normal' );
  }

}
add_action('wp_dashboard_setup', 'powerpack__remove_dashboard_widgets' );