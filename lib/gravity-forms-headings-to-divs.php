<?php
/*
 * Convert Gravity Forms headings to divs for better SEO and styling
 */

add_filter( 'gform_get_form_filter', function ( $form_string, $form ) {
    $form_string = preg_replace("/<h3 class='gform_title'>(.*?)<\/h3>/", "<div class='gform_title'>$1</div>", $form_string);
    $form_string = preg_replace("/<h2 class='gsection_title'>(.*?)<\/h2>/", "<div class='gsection_title'>$1</div>", $form_string);
    $form_string = preg_replace("/<h3 class='gf_progressbar_title'>(.*?)<\/h3>/s", "<div class='gf_progressbar_title'>$1</div>", $form_string);
    return $form_string;
}, 10, 2 );