<?php

// Disables the deletion of the cache under certain circumstances
add_action( 'wp_rocket_loaded', 'pp2_rocket_remove_all_purge_hooks' );

/**
 * Remove some of WP Rocket’s cache purging actions 
 * Prevents the cache from being unecessarily purged
 * @return void
 */
function pp2_rocket_remove_all_purge_hooks() {

  // WP core action hooks rocket_clean_domain() gets hooked into.
  $clean_domain_hooks = array(
    'user_register', // When a user is added
    'profile_update', // When a user is updated
    'deleted_user', // When a user is deleted
    'update_option_theme_mods_' . get_option( 'stylesheet' ), // When any theme modifications are updated
  );

  // Remove rocket_clean_domain() from core action hooks.
  foreach ( $clean_domain_hooks as $key => $handle ) {
    remove_action( $handle, 'rocket_clean_domain' );
  }

}