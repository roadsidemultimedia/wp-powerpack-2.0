<?php

// Prevent WordPress update emails from being sent
add_filter( 'auto_core_update_send_email', '__return_false' );

/* 
 * Allow uploading additional file types using WP media uploader
 */
add_filter('upload_mimes', 'pp2_upload_mime_whitelist');
function pp2_upload_mime_whitelist ( $mime_types ) {
	$mime_types['ico'] = 'image/x-icon';
	$mime_types['ogv'] = 'video/ogg';
	return $mime_types;
}