<?php
/* 
 * Prevents Google Apps Login plugin from adding a cookie to every request.
 * This filter sets it to only add it to login requests
 * See: https://wp-glogin.com/docs/api/login-hooks/#gal_set_login_cookie
 */

function pp2_gal_set_login_cookie($dosetcookie) {
  // Only set cookie on wp-login.php page
  return $GLOBALS['pagenow'] == 'wp-login.php';
}
add_filter('gal_set_login_cookie', 'pp2_gal_set_login_cookie');