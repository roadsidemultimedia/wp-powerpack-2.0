<?php
// Clear items that are older than a 180 days (i.e. keep a 6 month log), instead of only 60 days
add_filter( 'simple_history/db_purge_days_interval', function( $days ) {
    $days = 180;
    return $days;
});