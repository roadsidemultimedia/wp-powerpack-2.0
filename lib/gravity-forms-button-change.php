<?php
/*
 * Change Gravity Form submit input to an actual button so it can be styled easier
 */

add_filter( 'gform_next_button', 'pp2_gform_input_to_button', 10, 2 );
add_filter( 'gform_previous_button', 'pp2_gform_input_to_button', 10, 2 );
add_filter( 'gform_submit_button', 'pp2_gform_input_to_button', 10, 2 );
function pp2_gform_input_to_button( $button, $form ) {
    $dom = new DOMDocument();
    $dom->loadHTML( $button );
    $input = $dom->getElementsByTagName( 'input' )->item(0);
    $new_button = $dom->createElement( 'button' );
    $wrapper = $dom->createElement( 'span' );
    $wrapper->appendChild( $dom->createTextNode( $input->getAttribute( 'value' ) ) );
    $new_button->appendChild($wrapper);
    $input->removeAttribute( 'value' );
    foreach( $input->attributes as $attribute ) {
        $new_button->setAttribute( $attribute->name, $attribute->value );
    }
    $input->parentNode->replaceChild( $new_button, $input );

    return $dom->saveHtml( $new_button );
}

add_filter('get_search_form', 'pp2_gform_search_form_text');
function pp2_gform_search_form_text($text) {
     $text = str_replace('<input type="submit" id="searchsubmit" value="Search" />', '<button type="submit" id="searchsubmit">Search</button>', $text);
     return $text;
}