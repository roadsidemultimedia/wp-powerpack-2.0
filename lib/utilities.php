<?php

// Add classes for parent and ancestor page IDs
if( !function_exists( "pp_add_parent_body_classes" ) ){
	add_filter('body_class','pp_add_parent_body_classes');
	function pp_add_parent_body_classes( $classes ) {
		global $post;
		if ( is_page() ) {
			$ancestor_array = get_post_ancestors( get_the_ID() );
			foreach($ancestor_array as $ancestor_id){
				if($post->post_parent == $ancestor_id){
					$classes[] = "parent-page-{$ancestor_id}";
				} else {
					$classes[] = "ancestor-page-{$ancestor_id}";
				}
			}
		}
		return array_unique($classes);
	}
}

//FUNCTION TO TEST IF PAGE IS A CHILD OF A PARENT, HOWEVER MANY LEVELS UP
if( !function_exists( "is_tree" ) ){
	function is_tree($possible_parents) { // $pid = The ID of the page we're looking for pages underneath
		global $post; // load details about this page
		if(!is_array($possible_parents))
			$possible_parents = array($possible_parents);
		foreach($possible_parents as $pid){
			$anc = get_post_ancestors( $post->ID );
			foreach($anc as $ancestor) {
				if(is_page() && $ancestor == $pid) {
					return true;
				}
			}
			if(is_page()&&(is_page($pid))) 
				return true;   // we're at the page or at a sub page
		}
		return false;
	};
}

if(!function_exists("is_child")){
	function is_child($pid=Null)
	{
		if(!$pid)
		{
			global $post;
			$pid = $post->ID;
		}
		$ancestors = get_post_ancestors($pid);
		if(count($ancestors))
			return True;
		return False;
	}
}

//Checks for siblings
if(!function_exists("has_siblings")){
	function has_siblings($pid=Null)
	{
		if(!$pid)
		{
			global $post;
			$pid = $post->ID;
		}
		$ancestors = get_post_ancestors($pid);
		if(count($ancestors) < 1)
			return True;
		$parent_id = $ancestors[0];
		$siblings = get_posts(array(
			"post_type" => "page",
			"post_parent" => $parent_id
		));
		if(count($siblings)) return True;
		return False;
	}
}