<?php

/**
 * Provide a list of Gravity Forms capabilities
 * 
 * @access public
 * @return void
 */
function powerpack__get_gforms_cap_list(){
  // For a full list of Gravity Forms capabilities see: 
  // https://www.gravityhelp.com/documentation/article/role-management-guide/#gravity-forms-core-capabilities
  
  return [
    'gravityforms_view_entries',
    'gravityforms_edit_entries',
    'gravityforms_delete_entries',
    'gravityforms_view_entry_notes',
    'gravityforms_edit_entry_notes',
    'gravityforms_preview_forms',
  ];
}

/**
 * Add Gravity Forms capabilities to Author role.
 * add_cap writes to the database. It's important that it checks before writing, 
 * so it isn't making constant writes to the database on every page load.
 * 
 * @access public
 * @return void
 */
function powerpack__activate_gforms_cap() {

  $role = get_role( 'author' );
  $gform_roles = powerpack__get_gforms_cap_list();
  foreach ($gform_roles as $gform_role) {
    if( !isset($role->capabilities[$gform_role]) ){
      $role->add_cap( $gform_role );
    }
  }

}
// register_activation_hook( __FILE__, 'powerpack__activate_gforms_cap' );
add_action( 'admin_init', 'powerpack__activate_gforms_cap');

/**
 * Remove Gravity Forms capabilities from Author role.
 * Runs during plugin deactivation.
 * 
 * @access public
 * @return void
 */
function powerpack__deactivate_gforms_cap() {
 echo "<pre>Deactivation hook (powerpack__deactivate_gforms_cap) executed!!</pre>";
 $role = get_role( 'author' );
 $gform_roles = powerpack__get_gforms_cap_list();
 foreach ($gform_roles as $gform_role) {
   $role->remove_cap( $gform_role );
 }

}
register_deactivation_hook( WP_POWERPACK_INDEX_PATH, 'powerpack__deactivate_gforms_cap' );